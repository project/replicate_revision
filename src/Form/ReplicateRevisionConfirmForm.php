<?php
/**
 * Created by PhpStorm.
 * User: mohamed
 * Date: 25/07/2017
 * Time: 14:40
 */

namespace Drupal\replicate_revision\Form;


use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\replicate_ui\Form\ReplicateConfirmForm;


class ReplicateRevisionConfirmForm extends ReplicateConfirmForm {

  protected $routeMatch;

  protected $bundle;

  protected $entity_type_id;

  public function buildForm(array $form, FormStateInterface $form_state, RouteMatchInterface $route_match = NULL) {

    $this->routeMatch = $route_match;
    $this->entity_type_id = $this->getEntityTypeId();
    $this->bundle = $this->getEntity()->bundle();
    $entity_type = $this->entityTypeManager->getDefinition($this->getEntityTypeId());
    if($entity_type->isRevisionable()) {
      $form['is_replicate_to'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Replicate as a revsiosn to %type', ['%type' => $this->entity_type_id]),
      ];
      $form['replicate_to'] = [
        '#type' => 'entity_autocomplete',
        '#title' => $this->t('Replicate to'),
        '#target_type' => $this->entity_type_id,
        '#selection_settings' => [
          'target_bundles' => [$this->bundle],
        ],
        '#states' => [
          'visible' => [
            ':input[name="is_replicate_to"]' => ['checked' => TRUE],
          ]
        ]
      ];
    }
    return parent::buildForm($form, $form_state, $route_match);
  }
  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    if($form_state->getValue('is_replicate_to') && empty($form_state->getValue('replicate_to') )) {
      $form_state->setErrorByName('replicate_to', $this->t('You have to specify a valid entity to clone to.'));
    }
  }
  /**
   * {@inheritdoc}
   */

  public function submitForm(array &$form, FormStateInterface $form_state) {

    if(!$form_state->getValue('is_replicate_to') ) {
      parent::submitForm($form, $form_state);
      return;
    }
    $target_entity_id = $form_state->getValue('replicate_to');
    $source_entity = $this->getEntity();
    $target_entity = $this->entityManager->getStorage($this->entity_type_id)
      ->load($target_entity_id);
    $this->entityManager->getDefinition($this->entity_type_id);
    $target_entity->setTitle($source_entity->getTitle());
    foreach ($this->entityManager->getFieldDefinitions($this->entity_type_id, $this->bundle) as $field_name => $field_definition) {
      if (!empty($field_definition->getTargetBundle())) {
        $target_entity->{$field_name}->setValue($source_entity->{$field_name}->getValue());
      }
    }
    $target_entity->setNewRevision();
    $target_entity->save();
    drupal_set_message($this->t('%type (%id) has been replicated to id %new!', [
      '%type' => $this->entity_type_id,
      '%id' => $source_entity->id(),
      '%new' => $target_entity->id()]
    ));
    $form_state->setRedirectUrl($target_entity->toUrl());
  }

}