<?php

namespace Drupal\replicate_revision;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;

/**
 * Modifies the language manager service.
 */
class ReplicateRevisionServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    $definition = $container->getDefinition('replicate_ui.hooks.entity_type_alter');
    $definition->setClass('Drupal\replicate_revision\Hooks\EntityTypeAlter');
  }
}